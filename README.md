# QUOTES API 

A simle api to fetch quotes by author, based on symfony 4

## Requirements
* PHP >= 7.2 
* ext-json
    
## Installation

* clone the repo
* create local env `cp .env .env.local`
* run `composer install`

## Usage

**GET** `/shout/{author-name}?limit={limit}`

* **author-name** - required, string, e.g. steve-jobs, dalai-lama, etc.
* **limit** - optional, integer, between 1 and 10
    
## Testing

* run `php bin/phpunit`

    