<?php


namespace App\Repositories\Cache;

use App\Repositories\Data\QuotesDataRepository;
use App\Repositories\Interfaces\QuotesIntefrace;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Cache\InvalidArgumentException;

/**
 * Class QuotesCacheRepository
 * @package App\Repositories\Cache
 */
class QuotesCacheRepository implements QuotesIntefrace
{

    /**
     * @var QuotesDataRepository
     */
    protected $repository;
    /**
     * @var CacheItemPoolInterface
     */
    protected $cache;

    /**
     * QuotesCacheRepository constructor.
     *
     * @param QuotesDataRepository   $repository
     * @param CacheItemPoolInterface $cache
     */
    public function __construct(QuotesDataRepository $repository, CacheItemPoolInterface $cache)
    {
        $this->repository = $repository;
        $this->cache = $cache;

    }

    /**
     * @param string $author
     * @param int    $limit
     *
     * @return array
     * @throws InvalidArgumentException
     */
    public function getByAuthor(string $author, int $limit): array
    {
        $items = $this->cache->getItem( 'quotes_'.$author.'_'.$limit);

        if (!$items->isHit()) {
            $quotes = $this->repository->getByAuthor($author, $limit);

            $items->set($quotes);
            $this->cache->save($items);
        }

        return $items->get();
    }
}