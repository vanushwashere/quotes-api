<?php


namespace App\Repositories\Data;

use App\Repositories\Interfaces\QuotesIntefrace;


/**
 * Class QuotesDataRepository
 * @package App\Repositories\Data
 */
class QuotesDataRepository implements QuotesIntefrace
{
    /**
     *
     */
    public const PATH_TO_FILE =  __DIR__.'/../../../data/quotes.json';

    /**
     * @var
     */
    protected $data;

    /**
     * QuotesDataRepository constructor.
     */
    public function __construct()
    {
        $this->data = json_decode(file_get_contents(self::PATH_TO_FILE), true)['quotes'];
    }

    /**
     * @param string $author
     * @param int    $limit
     *
     * @return array
     */
    public function getByAuthor(string $author, int $limit): array
    {
        $author = $this->prepareString($author);
        $dataToReturn = [];
        foreach ($this->data as $item) {
            if (count($dataToReturn) === $limit) {
                break;
            }
            if (strtolower($item['author']) === $author) {
                $dataToReturn[] = $item['quote'];
            }
        }
        return $dataToReturn;
    }

    /**
     * @param string $author
     *
     * @return string
     */
    private function prepareString(string $author): string
    {
        return strtolower(str_replace('-',' ',$author));
    }


}