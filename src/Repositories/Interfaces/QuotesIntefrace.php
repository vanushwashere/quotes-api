<?php


namespace App\Repositories\Interfaces;


/**
 * Interface QuotesIntefrace
 * @package App\Repositories\Interfaces
 */
interface QuotesIntefrace  {
    /**
     * @param string $author
     * @param int    $limit
     *
     * @return mixed
     */
    public function getByAuthor(string $author, int $limit);
}